import asyncio


async def read_files(file_name):
    f = open(file_name)
    numbers = f.read()
    return numbers


async def divide(numbers):
    splited_num = numbers.split()
    return splited_num


async def take_action_num(splited_num):
    action = int(splited_num[0])
    return action


async def do_action(action, numbers):
    if action == 1:
        return sum(numbers[1:])
    if action == 2:
        result = 1
        for num in numbers[1:]:
            result *= num
        return result
    if action == 3:
        result = 0
        for num in numbers[1:]:
            result += num ** 2
        return result


async def from_str_to_num(numbers):
    result = [float(item) for item in numbers]
    return result


async def write_result(result):
    all_result = open('out.dat', "a")
    all_result.write(str(result) + " ")
    all_result.close()
    return all_result


async def read_write_to_file(file_name):
    reading_num = await read_files(file_name)
    splited_num = await divide(numbers=reading_num)
    action_number = await take_action_num(splited_num)
    true_numbers = await from_str_to_num(numbers=splited_num)
    all_result = await do_action(numbers=true_numbers, action=action_number)
    await write_result(result=all_result)


if __name__ == '__main__':
    open('out.dat', 'w')
    asyncio.run(read_write_to_file('in_1.dat'))
    asyncio.run(read_write_to_file('in_2.dat'))
    asyncio.run(read_write_to_file('in_3.dat'))
